# Awesome Gitea
[![Awesome](https://awesome.re/badge-flat.svg)](https://awesome.re) 
[![Contribution%20Guide](https://img.shields.io/badge/-Contribution%20Guide-informational?style=flat)](contributing.md)

A curated list of awesome projects using Gitea.

### Contents

- [Applications](#applications)
    - [Android](#android)
    - [Bot](#bot)
    - [Command Line](#command-line)
    - [DevOps](#devops)
    - [Storage](#storage)
<br><br>
- [Frameworks](#frameworks)
    - [Mobile](#mobile)
    - [Python](#python)
<br><br>
- [Organizations](#organizations)
<br><br>
- [Themes](#themes)

## Applications

### Android

* [Gadgetbridge](https://codeberg.org/Freeyourgadget/Gadgetbridge) - A free and cloudless replacement for your gadget vendors' closed source Android applications. 
* [GitNex](https://gitea.com/mmarif/GitNex) - Android client for Gitea.


### Bot

* [staletea](https://gitea.com/jonasfranz/staletea) - StaleBot for Gitea.

### Command Line

* [brew](https://gitea.com/Homebrew/brew) - The missing package manager for macOS (or Linux) (mirror).
* [mc](https://gitea.com/minio/mc) -MinIO Client is a replacement for ls, cp, mkdir, diff and rsync commands for filesystems and object storage (mirror).
* [tea](https://gitea.com/gitea/tea) - A command line tool to interact with Gitea servers.


### DevOps

* [gitea](https://gitea.com/gitea/gitea_mirror) - Git with a cup of tea, painless self-hosted git service (mirror).

### Storage

* [MinIO](https://gitea.com/minio/minio) - MinIO is a high performance object storage server compatible with Amazon S3 APIs (mirror).


<br>

## Frameworks

### Mobile

* [flutter](https://gitea.com/flutter/flutter) -  Flutter makes it easy and fast to build beautiful mobile apps (mirror).

### Python

* [biotite](https://codeberg.org/biotite-dev/biotite) - A general framework for computational biology (mirror).


<br>

## Organizations

* [Codeberg](https://codeberg.org/Codeberg) - Non-Profit Collaboration Community for Free and Open Source Projects (formerly known under its working title teahub.io).
* [OpenDev](https://opendev.org/) - A space for collaborative Open Source software development.
* [PSES](https://git.passageenseine.fr/pses) - Pas Sage en Seine.
* [Teknik](https://git.teknik.io/Teknikode) - Provide services to help those who try to innovate.

## Themes

* [Gitea Matrix Template](https://github.com/TylerByte666/gitea-matrix-template) - Custom Gitea Theme! Spruce up gitea's arc-green template, with a matrix inspired background. 
